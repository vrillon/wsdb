import axios from 'axios'

const APIDomain = 'https://api.wsdb.xyz/'

const api = axios.create({
  baseURL: APIDomain
})

export function loadNews (page, lang) {
  return api.get('api.php?action=news&lang=' + lang + '&page=' + page)
}

export function getFactions (lang) {
  return api.get('api.php?action=factions&lang=' + lang)
}

export function getFaction (lang, id) {
  return api.get('api.php?action=faction&lang=' + lang + '&id=' + id)
}

export function getBonuses (lang) {
  return api.get('api.php?action=bonuses&lang=' + lang)
}

export function getBonus (lang, id) {
  return api.get('api.php?action=bonus&lang=' + lang + '&id=' + id)
}

export function getClasses (lang) {
  return api.get('api.php?action=classes&lang=' + lang)
}

export function getClass (lang, id) {
  return api.get('api.php?action=class&lang=' + lang + '&id=' + id)
}

export function getSkills (lang) {
  return api.get('api.php?action=skills&lang=' + lang)
}

export function getSkill (lang, id) {
  return api.get('api.php?action=skill&lang=' + lang + '&id=' + id)
}

export function getIconURL (id) {
  return `${APIDomain}imgs/icons/${id}.png`
}

export function getItems (lang, category) {
  return api.get('api.php?action=items&lang=' + lang + '&id=' + category)
}

export function getCalculator (lang) {
  return api.get('api.php?action=calculator&lang=' + lang)
}

export function getSlotItems (lang, clazz, slot, level) {
  return api.get(`api.php?action=slot-items&class=${clazz}&slot=${slot}&level=${level}&lang=${lang}`)
}

export function getItem (lang, id) {
  return api.get('api.php?action=item&lang=' + lang + '&id=' + id)
}

export function getClassInfo (clazz, level) {
  return api.get('api.php?action=class-base&class=' + clazz + '&level=' + level)
}

export function getEnchants (lang, item, sub, level, type) {
  return api.get(`api.php?action=enchants&lang=${lang}&item-type=${item}&item-sub=${sub}&level=${level}&type=${type}`)
}

export function bonusValue ({ percent, value }, purified = false) {
  if (purified) {
    if (percent) {
      return (value / 100).toFixed(2) + '%'
    }
    return (value / 100).toFixed(1)
  }
  if (percent) {
    return (Math.floor(value / 10) / 10).toFixed(1) + '%'
  }
  return Math.floor(value / 100)
}

const h1 = [
  [1, 0], [1.02, 1], [1.04, 2], [1.06, 3], [1.1, 4], [1.14, 5], [1.21, 6], [1.33, 7], [1.49, 8], [1.66, 9], [1.86, 10]
]

const h2 = [
  [1, 0], [1.02, 2], [1.04, 4], [1.06, 6], [1.1, 8], [1.14, 10], [1.21, 12], [1.33, 14], [1.49, 16], [1.66, 18], [1.86, 20]
]

const arm = [
  [1, 0], [1.06, 10], [1.1, 14], [1.15, 18], [1.24, 22], [1.37, 26], [1.53, 30], [1.72, 34], [1.98, 38], [2.34, 42], [2.85, 46]
]

const jew = [
  [1, 0], [1.11, 10], [1.18, 14], [1.25, 18], [1.35, 22], [1.47, 26], [1.63, 30], [1.85, 34], [2.15, 38], [2.52, 42], [3, 46]
]

function powerUp (tab, value, power) {
  return tab[power][0] * value + tab[power][1] * 100
}
export function powerUpItem (type, sub, value, power) {
  switch (type << 16 | sub) {
    case 1:
    case 2:
    case 10:
    case 222:
      return powerUp(h1, value, power)
    case 78:
    case 76:
    case 81:
    case 223:
    case 65557:
    case 65570:
    case 65633:
      return powerUp(h2, value, power)
    // case 196648: -- shield
    case 262144:
    case 589824:
    case 655360:
      return powerUp(jew, value, power)
    default:
      return powerUp(arm, value, power)
  }
}

export const emptyItem = {
  id: 0,
  icon: null,
  percent: false,
  extra: false,
  value: 0,
  name: ''
}

export const itemCooldown = {
  1: 2.2,
  2: 2.0,
  10: 1.7,
  76: 3.2,
  78: 3.2,
  81: 3.2,
  222: 2.4,
  223: 3.4,
  65557: 3.1,
  65570: 3.3,
  65633: 3.9
}

export function itemColor (bind, color, relic = false) {
  if ((bind & 64) > 0) {
    return 6
  }
  if ((bind & 1) > 0) {
    return 7
  }
  if (relic) {
    return 8
  }
  return color
}

export const relicCategories = [73, 74, 75, 76]

export const hasLook = [0, 1, 2, 3, 4, 5, 7, 8]
