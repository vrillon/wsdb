const fs = require('fs')
const path = require('path')
const webpack = require('webpack')
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin')

module.exports = {
  pages: {
    index: {
      entry: './src/main.js',
      template: 'public/index.html',
      title: 'Warspear Database'
    }
  },
  filenameHashing: true,
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        _CLOUDS_: JSON.stringify(fs.readdirSync('src/assets/clouds').map((f) => f.split('.')[0]))
      }),
      new ServiceWorkerWebpackPlugin({
        entry: path.join(__dirname, 'src/worker/sw.js'),
        filename: 'sw.js',
        excludes: [
          '**/.*',
          '**/*.map',
          '*.html'
        ]
      })
    ]
  },
  devServer: {
    proxy: {
      '^/api.php': {
        target: 'http://localhost/',
        changeOrigin: true
      }
    },
    historyApiFallback: true
  }
}
